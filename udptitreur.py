#!/usr/bin/env python

from __future__ import print_function
import sys, select
import socket
import random
import signal
from threading import Timer,Thread,Event
from subprocess import Popen, PIPE, STDOUT

RUN = True

def signal_handler(signal, frame):
	global RUN
	RUN = False
	sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)

# GET UPD PORT
if len(sys.argv) < 2:
	print ("Error: you should specify a PORT")
	exit (1)

UDP_PORT = int(sys.argv[1])

# START HARDWARE
proc = Popen(['/root/udptitreur/hardware6'], stdout=PIPE, stdin=PIPE, stderr=PIPE)

y=select.poll()
y.register(proc.stdout,select.POLLIN)

line = proc.stdout.readline().strip()
if line != "#INITHARDWARE":
	print (line)
	print ("can't start hardware communication")
	exit (1)

print ("Initializing hardware..")
proc.stdin.write(b'initconfig -carteVolt ? -name yo -titreurNbr 6 -manualmode 1\n')
proc.stdin.flush()
line = proc.stdout.readline().strip()
if line != "#HARDWAREREADY":
	print ("can't init hardware")
	exit (1)
print ("Hardware Initialized")


# UDP SERVER
sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP
sock.bind(("0.0.0.0", UDP_PORT))
sock.settimeout(0.1)
print("Receiveing OSC on", UDP_PORT)

SPEEDMIN = 20000
SPEEDMAX = 45000

SCROLLSPEED = 60

MODES = {}
MODES['NO_SCROLL_NORMAL'] = 0
MODES['SCROLL_NORMAL'] = 1
MODES['SCROLL_LOOP_NORMAL'] = 2
MODES['SCROLL_VERTICAL_NORMAL'] = 11		# broken
MODES['SCROLL_VERTICAL_LOOP_NORMAL'] = 12	# broken
MODES['NO_SCROLL_BIG'] = 100
MODES['SCROLL_BIG'] = 101
MODES['SCROLL_LOOP_BIG'] = 102				# broken
MODES['SCROLL_VERTICAL_BIG'] = 111			# broken
MODES['SCROLL_VERTICAL_LOOP_BIG'] = 112		# broken

TEXTS = [
	('<3 <3 <3 BEAUCOUP BEAUCOUP PLUS DE BISOUS BIEN MOITES <3 <3 <3 <3 <3 <3', 'SCROLL_BIG'),
	('<3 <3 <3 BEAUCOUP BEAUCOUP PLUS DE BOUCHES DE LANGUES QUI TOURNENT DANS TOUS LES SENS <3 <3 <3 <3 <3 <3', 'SCROLL_BIG'),
	('<3 <3 <3 BEAUCOUP BEAUCOUP PLUS DE CHALEUR DE SERRES ET CORPS CHAUDS <3 <3 <3 <3 <3 <3', 'SCROLL_BIG'),
	('<3 <3 <3 BEAUCOUP BEAUCOUP PLUS DE DANSE ! DANSE ! DANSE ZARMA ! <3 <3 <3 <3 <3 <3', 'SCROLL_BIG'),
	('<3 <3 <3 BEAUCOUP BEAUCOUP PLUS DE DEBOUT SUR LES VOITURES <3 <3 <3 <3 <3 <3', 'SCROLL_BIG'),
	('<3 <3 <3 NOUS SOMMES DES ANIMALS <3 <3 <3 <3 <3 <3', 'SCROLL_BIG'),
	('<3 <3 <3 BEAUCOUP BEAUCOUP PLUS DE GRIMPE DANS LES ARBRES <3 <3 <3 <3 <3 <3', 'SCROLL_BIG'),
	('<3 <3 <3 BEAUCOUP BEAUCOUP PLUS SAUTE SUR LES ABRIS BUS <3 <3 <3 <3 <3 <3', 'SCROLL_BIG'),
	('<3 <3 <3 BEAUCOUP BEAUCOUP PLUS DE GRIMPE SUR LES POUBELLES <3 <3 <3 <3 <3 <3', 'SCROLL_BIG'),
	('<3 <3 <3 BEAUCOUP BEAUCOUP PLUS DE CRIS D ANIMALS <3 <3 <3 <3 <3 <3', 'SCROLL_BIG'),
	('<3 <3 <3 NOUS SOMMES DES ANIMALS <3 <3 <3 <3 <3 <3', 'SCROLL_BIG'),
	('<3 <3 <3 BEAUCOUP BEAUCOUP PLUS DE SONS GUTTURALS <3 <3 <3 <3 <3 <3', 'SCROLL_BIG'),
	('<3 <3 <3 BEAUCOUP BEAUCOUP PLUS LA TETE DANS LES BOCALS <3 <3 <3 <3 <3 <3', 'SCROLL_BIG'),
	('<3 <3 <3 BEAUCOUP BEAUCOUP DE HURLEMENTS DE JOIE ET DAMOURS <3 <3 <3 <3 <3 <3', 'SCROLL_BIG'),
	('<3 <3 <3 BEAUCOUP BEAUCOUP PLUS DE GRRRR ET DE HAAAAAA ET DE YOULOUYOULOUYOULOU <3 <3 <3 <3 <3 <3', 'SCROLL_BIG'),
	('<3 <3 <3 NOUS SOMMES DES ANIMALS <3 <3 <3 <3 <3 <3', 'SCROLL_BIG'),
	('<3 <3 <3 CRIE BEAUCOUP CRIE BEAUCOUP CRIE BEAUCOUP PLUS <3 <3 <3 <3 <3 <3', 'SCROLL_BIG'),
	('<3 <3 <3 DANSE BEAUCOUP DANSE BEAUCOUP DANSE BEAUCOUP PLUS <3 <3 <3 <3 <3 <3', 'SCROLL_BIG'),
	('<3 <3 <3 ICI ET MAINTENANT Y A QUE CA Y A QUE CA <3 <3 <3 <3 <3 <3', 'SCROLL_BIG'),
	('<3 <3 <3 SAUTE BEAUCOUP SAUTE BEAUCOUP SAUTE BEAUCOUP PLUS <3 <3 <3 <3 <3 <3', 'SCROLL_BIG'),
	('<3 <3 <3 COURS BEAUCOUP COURS BEAUCOUP COURS BEAUCOUP PLUS <3 <3 <3 <3 <3 <3', 'SCROLL_BIG'),
	('<3 <3 <3 NOUS SOMMES DES ANIMALS <3 <3 <3 <3 <3 <3', 'SCROLL_BIG'),
	('<3 <3 <3 AIME BEAUCOUP AIME AIME BEAUCOUP BEAUCOUP <3 <3 <3 <3 <3 <3', 'SCROLL_BIG'),
	('<3 <3 <3 LAISSE LA VIE TE DEBORDER BEAUCOUP BEAUCOUP <3 <3 <3 <3 <3 <3', 'SCROLL_BIG'),
	('<3 <3 <3 LA RAGE PUTAIN LA RAGE BEAUCOUP BEAUCOUP <3 <3 <3 <3 <3 <3', 'SCROLL_BIG'),
	('<3 <3 <3 ON VA PAS SE LAISSER EMMERDER BEAUCOUP BEAUCOUP <3 <3 <3 <3 <3 <3', 'SCROLL_BIG'),
	('<3 <3 <3 ON EST JEUNES GRANDS BELLES ET BEAUX BEAUCOUP BEAUCOUP <3 <3 <3 <3 <3 <3', 'SCROLL_BIG'),
	('<3 <3 <3 NOUS SOMMES DES ANIMALS <3 <3 <3 <3 <3 <3', 'SCROLL_BIG'),
	('<3 <3 <3 AMOUR AMOUR AMOUR BEAUCOUP BEAUCOUP <3 <3 <3 <3 <3 <3', 'SCROLL_BIG'),
	('<3 <3 <3 LAISSE TES PIEDS TAPER LE BITUME ET TES JAMBES FOUTRE LE CAMP BEAUCOUP BEAUCOUP <3 <3 <3 <3 <3 <3', 'SCROLL_BIG'),
	('<3 <3 <3 LAISSE TON CORPS TOUT DECHIRER DANS LA STREET BEAUCOUP BEAUCOUP <3 <3 <3 <3 <3 <3', 'SCROLL_BIG'),
	('<3 <3 <3 NOUS SOMMES LA VIE ZARMA NOUS SOMMES LA VIE BEAUCOUP BEAUCOUP <3 <3 <3 <3 <3 <3', 'SCROLL_BIG'),
	('<3 <3 <3 NOUS SOMMES DES ANIMALS <3 <3 <3 <3 <3 <3', 'SCROLL_BIG'),
	('<3 <3 <3 NOUS SOMMES DES ANIMALS BEAUCOUP BEAUCOUP <3 <3 <3 <3 <3 <3', 'SCROLL_BIG'),
	('<3 <3 <3 ICI ET MAINTENANT Y A QUE CA Y A QUE CA <3 <3 <3 <3 <3 <3', 'SCROLL_BIG'),
	('<3 <3 <3 BEAUCOUP BEAUCOUP <3 <3 <3 <3 <3 <3', 'SCROLL_BIG'),
	('<3 <3 <3 EMBRASSE EMBRASSE BEAUCOUP BEAUCOUP <3 <3 <3 <3 <3', 'SCROLL_BIG')
]

LASTITEM = ()

# PICK FROM LIST (avoid last item)
def choice_without_repetition(lst):
	if len(lst) == 0:
		return None
	elif len(lst) == 1:
		return lst[0]
	else:
		while True:
			global LASTITEM
			item = random.choice(lst)
			if item != LASTITEM:
				LASTITEM = item
				return item

# DISPLAY EVERY x SECONDS
def pickAndShow():
	print ("tick")
	item = choice_without_repetition(TEXTS)
	if item:
		print ("Display", item)

		proc.stdin.write(b'flushtitreur\n')
		proc.stdin.flush()

		txt = item[0].split('/')
		cmd = 'texttitreur'
		cmd += ' -line1 ' + txt[0].replace(' ', '_')
		if len(txt) > 1:
			cmd += ' -line2 ' + txt[1].replace(' ', '_')

		if item[1] in MODES.keys():
			cmd += ' -type ' + item[1]
		else:
			cmd += ' -type NO_SCROLL_NORMAL'

		cmd += ' -speed ' + str(SCROLLSPEED)
		cmd += '\n'

		while y.poll(1):
			print (" -- hardware6 says:", proc.stdout.readline())
			#sys.exit(1)

		print (cmd)
		proc.stdin.write(cmd)
		proc.stdin.flush()

	time = random.randint(SPEEDMIN,SPEEDMAX)/1000.0
	if RUN:
		theTimer = Timer(time, pickAndShow)
		theTimer.start()
		print ("next tick in ",time)


theTimer = Timer(0.1, pickAndShow)
theTimer.start()

while RUN:
	try:
		data, address = sock.recvfrom(4096)
		data = data.split(' ')
		print ("REceived", data, "from", address)

		# speed TIMEMIN [TIMEMAX]
		if data[0] == "speed":
			if len(data) >= 3 and data[2] != "#":
				SPEEDMIN = int(data[1])
				SPEEDMAX = int(data[2])
			elif len(data) >= 2:
				SPEEDMIN = int(data[1])
				SPEEDMAX = int(data[1])
			print ("new speed: ", SPEEDMIN, SPEEDMAX)

		# scrool speed
		elif data[0] == "scrollspeed":
			if len(data) >= 2:
				SCROLLSPEED = int(data[1])
				print ("new size: ", SCROLLSPEED)

		# clear
		elif data[0] == "clear":
			TEXTS = []
			print ("clear texts")

		# add text
		elif data[0] == "add":
			if len(data) >= 3:
				mode = data[1]
				txt = "_".join(data[2:])
				TEXTS.append((txt,mode))
				print ("add text", txt, mode)

		# text
		elif data[0] == "text":
			if len(data) >= 3:
				TEXTS = []
				mode = data[1]
				txt = "_".join(data[2:])
				TEXTS.append((txt,mode))
				print ("1 text", txt, mode)
			else:
				print ("empty txt", data)

		# tick
		elif data[0] == "tick":
			theTimer.cancel()
			theTimer = Timer(0.1, pickAndShow)
			theTimer.start()


	except socket.timeout:
		pass
